import { async, fakeAsync, flush, TestBed, tick, waitForAsync } from '@angular/core/testing';

import { UserService } from './user.service';

describe('UsersService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /**
   * A basic sync call test.
   */
  it('#getUsers should return the list of users', () => {
    // We don't need given (we are not in a real project).
    // But you might need a given step to mock an HTTP call for example.

    // When
    const result = service.getUsers();

    // Then
    expect(result).toBeDefined();
    expect(result.length).toEqual(2);
  });

  /**
   * This test will not fail itself. But it is wrong !
   * Usage of xit to disable the test.
   */
  xit('#getUsersAsync should return the list of users', () => {
    // We don't need given (we are not in a real project).
    // But you might need a given step to mock an HTTP call for example.

    // When
    service.getUsersAsync().subscribe((result) => {
      // Then
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
    });
  });

  /**
   * This is the right way to test an async call.
   * Use waitForAsync (or async in previous versions).
   */
  it('#getUsersAsync should return the list of users', waitForAsync(() => {
    // We don't need given (we are not in a real project).
    // But you might need a given step to mock an HTTP call for example.

    // When
    service.getUsersAsync().subscribe((result) => {
      // Then
      expect(result).toBeDefined();
      expect(result.length).toEqual(2);
    });
  }));

  it('#getUser should return the user Alice', () => {
    // Given
    const userId = 0;

    // When
    const result = service.getUser(userId);

    // Then
    expect(result).toBeDefined();
    expect(result.name).toEqual('Alice');
  });

  it('#getUser should throw an error when user not found', () => {
    // Given
    const userId = 3;

    // When
    try {
      service.getUser(userId);
    } catch (e) {
      // Then
      expect(e).toBeDefined();
      expect(e.message).toEqual('User not found');
    };
  });

  it('#getUserAsync should return the user Alice', waitForAsync(() => {
    // Given
    const userId = 0;

    // When
    service.getUserAsync(userId).subscribe(user => {
      // Then
      expect(user).toBeDefined();
      expect(user.name).toEqual('Alice');
    });
  }));

  it('#getUserAsync should not return the user Alice after some delay', fakeAsync(() => {
    // Given
    const userId = 0;
    service.mimicUserChangeEvent(); // We mimic an event in 500ms
    tick(500); // We go forward of 500ms

    // When
    service.getUserAsync(userId).subscribe(user => {
      // Then
      expect(user).toBeDefined();
      expect(user.name).toEqual('NotAlice');
    });
  }));

  it('#getUserAsync should throw an error when user not found', waitForAsync(() => {
    // Given
    const userId = 3;

    // When
    service.getUserAsync(userId).subscribe(user => {
      throw new Error('Should be an error');
    },
    error => {
      // Then
      expect(error).toBeDefined();
      expect(error.message).toEqual('User not found');
    });
  }));
});
