import { Injectable } from '@angular/core';
import { Observable, of, throwError, timer } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';
import { User } from 'unit-test-shared/models';
/**
 * The users service.
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  /**
   * The map of users.
   */
  private users: Map<number, User>;

  /**
   * Constructor.
   * Initializing users.
   */
  constructor() {
    this.users = new Map<number, User>();
    this.users.set(0, { id: 0, name: 'Alice', role: 'admin' });
    this.users.set(1, { id: 1, name: 'Bob', role: 'hacker'});
  }

  /**
   * Getter for all the users.
   */
  public getUsers(): Array<User> {
    return Array.from(this.users.values());
  }

  /**
   * Getter for all the users (async way).
   */
  public getUsersAsync(): Observable<Array<User>> {
    return of(this.getUsers());
  }

  /**
   * Getter for one user by its id.
   * Will throw an error if not found.
   * @param id the id of the user
   */
  public getUser(id: number): User {
    const user = this.users.get(id);
    if (user) {
      return user;
    }
    throw new Error('User not found');
  }

  /**
   * Getter for one user by its id (async way).
   */
  public getUserAsync(id: number): Observable<User> {
    try {
      return of(this.getUser(id));
    } catch(e) {
      return throwError(e);
    }
  }

  /**
   * Utility method to mimic a change in user 0.
   */
  public mimicUserChangeEvent(): void {
    setTimeout(() => {
      this.getUser(0).name = 'NotAlice';
    }, 500);
  }
}
