import { Component, OnInit } from '@angular/core';
import { UserService } from 'unit-test-core/services';
import { User } from 'unit-test-shared/models';

/**
 * The home component.
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  /**
   * The user list.
   */
  users: Array<User> = [];

  /**
   * Constructor.
   * @param userService the user service
   */
  constructor(private userService: UserService) { }

  /**
   * On init hook.
   * Will get the user list.
   */
  ngOnInit(): void {
    this.users = this.userService.getUsers();
  }

  /**
   * Say hi to the given name.
   * @param name the name to include in the message
   */
  sayHi(name: string): string {
    return `Hi ${name}`;
  }
}
