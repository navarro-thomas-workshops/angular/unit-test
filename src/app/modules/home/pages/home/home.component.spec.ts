import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from 'unit-test-core/services';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let userService: UserService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [ UserService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService);
    fixture.detectChanges();
  });

  it('#ngOnInit component should be created', () => {
    expect(component).toBeTruthy();
  });

  // Component testing API mode.

  it('#ngOnInit component should not have users when service returns empty', () => {
    // Given
    spyOn(userService, 'getUsers').and.returnValue([]); // We mock the return of the getUsers method

    // When
    component.ngOnInit();
    fixture.detectChanges(); // We refresh the view

    // Then
    expect(component.users).toBeDefined();
    expect(component.users.length).toEqual(0);
  });

  it('#ngOnInit component should have users when service returns a list', () => {
    // No given as we want the default value.

    // When
    component.ngOnInit();
    fixture.detectChanges(); // We refresh the view

    // Then
    expect(component.users).toBeDefined();
    expect(component.users.length).toEqual(2);
  });

  // Component testing DOM mode.

  it('#ngOnInit component should not display users when service returns empty', () => {
    // Given
    spyOn(userService, 'getUsers').and.returnValue([]); // We mock the return of the getUsers method

    // When
    component.ngOnInit();
    fixture.detectChanges(); // We refresh the view

    // Then
    const element: HTMLElement = fixture.nativeElement;
    const isUserListFilled = element.getElementsByTagName('li').length > 0; // We get li elements by the tag name.
    expect(isUserListFilled).toBeFalse();
  });

  it('#ngOnInit component should display users when service returns a list', () => {
    // No given as we want the default value.

    // When
    component.ngOnInit();
    fixture.detectChanges(); // We refresh the view

    // Then
    const element: HTMLElement = fixture.nativeElement;
    const userList = element.getElementsByTagName('li'); // We get li elements by the tag name.
    expect(!!userList).toBeTrue();
    expect(userList?.length).toEqual(2);
  });
});
